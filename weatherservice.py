from functools import reduce

import requests
import datetime as dt
from datetime import timedelta
import pytz
from cache import Cache
from sun import Sun


class WeatherService(object):
    classvar = 5

    def __init__(self, config):
        self.config = config

        self.tz_info = pytz.timezone(config.timezone())

        city_id = config.owm_city_id()
        api_key = config.owm_api_key()
        self.coords = {'latitude': config.coords_lat(), 'longitude': config.coords_lon()}
        self.sun = Sun()

        self.current_weather_url = "http://openweathermap.org/data/2.5/weather?id={}&appid={}".format(city_id, api_key)
        self.forecast_url = "http://openweathermap.org/data/2.5/forecast?id={}&appid={}".format(city_id, api_key)

        self.next_update = 0
        self.cache = Cache(60 * config.api_refresh_interval())

        self.icons = {
            'drizzle': 'cloud_rain',
            'rain': 'cloud_rain',
            'thunderstorm': 'lightning_cloud',
            'snow': 'snow_flake',
            'mist': 'cloud_mist',
            'fog': 'cloud_mist',
            'haze': 'cloud_mist',
            'clear': 'clear_{}',
            'clouds': 'cloud',
            'clouds_few': 'partial_cloud_{}',
        }

    def __utc_ms_to_tz(self, utc_ms):
        utc_datetime = dt.datetime.utcfromtimestamp(utc_ms)
        return utc_datetime.replace(tzinfo=pytz.timezone('UTC')).astimezone(self.tz_info)

    """
    Takes in the raw API data and only returns the temperatures of the last 24 hours
    """

    def __find_relevant_temps(self, data):
        horizon = dt.datetime.today().astimezone(self.tz_info) + timedelta(hours=24)
        return map(lambda x: x['main']['temp'],
                   filter(lambda x: self.__utc_ms_to_tz(x['dt']) <= horizon, data['list']))

    def __find_min_temp(self, data):
        return reduce(lambda a, b: a if a < b else b, self.__find_relevant_temps(data))

    def __find_max_temp(self, data):
        return reduce(lambda a, b: a if a > b else b, self.__find_relevant_temps(data))

    def __get_api_data(self, url):
        if self.cache.exists(url):
            return self.cache.get(url)
        else:
            answer = requests.get(url).json()
            self.cache.put(url, answer)
            return answer

    """
    Get the current outside temperature.
    """

    def get_current_temp(self):
        result = self.__get_api_data(self.current_weather_url)
        return result['main']['temp']

    """
    Get the current outside temperature, formatted.
    """

    def get_current_temp_str(self):
        temp = int(self.get_current_temp())
        return (" {}°C" if 0 <= temp < 10 else "{}°C").format(str(temp))

    """
    Get the highest predicted temperature in the next 24h.
    """

    def get_high_temp(self):
        result = self.__get_api_data(self.forecast_url)
        return self.__find_max_temp(result)

    """
    Get the lowest predicted temperature in the next 24h.
    """

    def get_low_temp(self):
        result = self.__get_api_data(self.forecast_url)
        return self.__find_min_temp(result)

    """
    Converts the sun.py format to a datetime
    """

    def __sun_fmt_to_datetime(self, now, sun_fmt):
        fmt = '%Y-%m-%dT{:02d}:{:02d}:%S.000000+00:00'
        return dt.datetime.fromisoformat(now.strftime(fmt).format(int(sun_fmt['hr']), int(sun_fmt['min'])))

    """
    Returns d if we're currently between sunrise and sunset.
    Returns n if we're currently between sunset and sunrise.
    Defaults to d
    """

    def is_night(self):
        return self.get_day_night_id() == 'n'

    def get_day_night_id(self):
        now = dt.datetime.now(pytz.utc)
        sunset = self.__sun_fmt_to_datetime(now, self.sun.getSunsetTime(self.coords))
        sunrise = self.__sun_fmt_to_datetime(now, self.sun.getSunriseTime(self.coords))

        if sunset < now or now < sunrise:
            return 'n'  # during the night
        elif sunrise < now < sunset:
            return 'd'  # during the day
        else:
            return 'd'

    def get_weather_icon(self):
        result = self.__get_api_data(self.current_weather_url)
        weather_main = result['weather'][0]['main'].lower()
        weather_description = result['weather'][0]['description'].lower()

        if weather_main == 'clouds' and 'few' in weather_description:
            weather_main += '_few'

        if weather_main not in self.icons:
            return self.config.path('res/void.png')
        return self.config.path('res/' + self.icons[weather_main] + '.png').format(self.get_day_night_id())

    def get_temp_direction_icon(self):
        current_temp = float(self.get_current_temp())
        result = self.__get_api_data(self.forecast_url)
        next_temp = float(result['list'][0]['main']['temp'])
        next_next_temp = float(result['list'][1]['main']['temp'])
        if next_temp < current_temp - 0.5 > next_next_temp:
            return self.config.path('res/sink.png')
        elif next_temp > current_temp + 0.5 < next_next_temp:
            return self.config.path('res/rise.png')
        else:
            return self.config.path('res/same.png')

