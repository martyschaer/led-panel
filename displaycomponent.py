class DisplayComponent(object):
    def __init__(self, config):
        self.config = config

    def draw(self, canvas, area, animation_counter=0):
        raise NotImplementedError("draw is not implemented.")
