import datetime

from displaycomponent import DisplayComponent
from textutils import TextUtils


class ClockComponent(DisplayComponent):

    def __init__(self, config, weather_service):
        super(ClockComponent, self).__init__(config)
        self.weather_service = weather_service
        self.text = TextUtils(config)

    def draw(self, canvas, area, animation_counter=0):
        x = area['x']
        y = area['y']
        w = area['w']
        h = area['h']

        brightness = 0.4 if self.weather_service.is_night() else 1

        if w == 16 and h == 32:
            now = datetime.datetime.now()
            hour = now.strftime("%H")
            minute = now.strftime("%M")
            week_day = now.strftime("%a")
            month_day = now.strftime("%d")
            month = now.strftime("%b")

            self.text.draw_text(canvas, x + 3, y + 1, hour, brightness)
            self.text.draw_text(canvas, x + 3, y + 7, minute, brightness)
            self.text.draw_text(canvas, x + 3, y + 13, week_day, brightness)
            self.text.draw_text(canvas, x + 3, y + 19, month_day, brightness)
            self.text.draw_text(canvas, x + 3, y + 25, month, brightness)
        elif w == 32 and h == 16:
            self.text.draw_text_centered(canvas, x, y, w, h, datetime.datetime.now().strftime("%H:%M"), brightness)
        elif w == 16 and h == 16:
            now = datetime.datetime.now()
            hour = now.strftime("%H")
            minute = now.strftime("%M")
            self.text.draw_text(canvas, x + 1, y + 1, hour, brightness)
            self.text.draw_text(canvas, y + 1, y + 7, minute, brightness)
        else:
            raise NotImplementedError('The area', area, 'is not implemented for', self.__class__.__name__)


