"""
Small Cache which has a time to live for each entry.
I use this for only asking the APIs every few minutes instead of at every draw.
This problem could be solved with a more elegant architecture though.
"""
import time


class Cache(object):
    def __init__(self, ttl):
        self.__ttl = ttl
        self.__cache = dict()

    def exists(self, key):
        return key in self.__cache and self.__cache[key]['ttl'] > time.time()

    def put(self, key, value):
        self.__cache[key] = {'ttl': time.time() + self.__ttl, 'val': value}

    def get(self, key):
        if not self.exists(key):
            return None
        return self.__cache[key]['val']
