"""
Class which draws text to the matrix.
"""
from rgbmatrix import graphics


class TextUtils(object):
    def __init__(self, config):
        self.font = graphics.Font()
        self.font.LoadFont(config.font())

    """
    Draws the given text to the matrix at the given coordinates.
    """
    def draw_text(self, canvas, from_x, from_y, text, brightness=1.0, r=255, g=255, b=255):
        color = self.__create_color(brightness, r, g, b)

        graphics.DrawText(canvas, self.font, from_x, from_y + 5, color, text)

    """
    Draws the given text to the matrix in the given bounding box.
    """
    def draw_text_centered(self, canvas, from_x, from_y, w, h, text, brightness=1.0, r=255, g=255, b=255):
        color = self.__create_color(brightness, r, g, b)

        text_w = len(text) * 3 + (len(text) - 1)
        text_h = 5

        x_margin = int((w - text_w) / 2) + from_x
        y_margin = int((h - text_h) / 2) + from_y + text_h

        graphics.DrawText(canvas, self.font, x_margin, y_margin, color, text)

    def __create_color(self, brightness=1.0, r=255, g=255, b=255):
        return graphics.Color(r * brightness, g * brightness, b * brightness)
