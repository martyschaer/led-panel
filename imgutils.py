"""
Class which draws pictures to the matrix.
"""
from PIL import Image
import numpy as np


class ImgUtils(object):
    def __init__(self, config):
        self.config = config

    def __load_img(self, path):
        img = Image.open(path).convert('RGB')
        return {'w': img.width, 'h': img.height, 'data': np.array(img)}

    """
    Draw the image centered within the given area.
    Your given image must be <= than the given area.
    """

    def draw_img_centered(self, canvas, from_x, from_y, w, h, img_path, brightness=1.0):
        img = self.__load_img(img_path)
        img_w = img['w']
        img_h = img['h']

        if img_w > w or img_h > h:
            print("[ERR] Attempting to draw a picture that's too large for the matrix:", img_path)
            return

        x_margin = int((w - img_w) / 2) + from_x
        y_margin = int((h - img_h) / 2) + from_y

        for x in range(0, img['w']):
            for y in range(0, img['h']):
                r = img['data'][y][x][0] * brightness
                g = img['data'][y][x][1] * brightness
                b = img['data'][y][x][2] * brightness
                canvas.SetPixel(x + x_margin, y + y_margin, r, g, b)
