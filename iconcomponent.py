import datetime

from displaycomponent import DisplayComponent
from textutils import TextUtils
from imgutils import ImgUtils


class IconComponent(DisplayComponent):

    def __init__(self, config, weather_service):
        super(IconComponent, self).__init__(config)
        self.img = ImgUtils(config)
        self.weather_service = weather_service

    def draw(self, canvas, area, animation_counter=0):
        x = area['x']
        y = area['y']
        w = area['w']
        h = area['h']

        brightness = 0.4 if self.weather_service.is_night() else 1

        # draw weather icon
        icon = self.weather_service.get_weather_icon()
        self.img.draw_img_centered(canvas, x, y, w, h, icon, brightness)
