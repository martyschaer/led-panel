import datetime

from displaycomponent import DisplayComponent
from textutils import TextUtils
from imgutils import ImgUtils


class TemperatureComponent(DisplayComponent):

    def __init__(self, config, weather_service):
        super(TemperatureComponent, self).__init__(config)
        self.text = TextUtils(config)
        self.img = ImgUtils(config)
        self.weather_service = weather_service

    def draw(self, canvas, area, animation_counter=0):
        x = area['x']
        y = area['y']
        w = area['w']
        h = area['h']

        brightness = 0.4 if self.weather_service.is_night() else 1

        # draw temperature text
        content = self.weather_service.get_current_temp_str()
        self.text.draw_text(canvas, x + 1, y + 4, content, brightness)

        # animate temperature direction
        num_animation_steps = 3
        animation_step = animation_counter % num_animation_steps
        icon = self.weather_service.get_temp_direction_icon()

        xoff = x + 9
        yoff = y + 10
        if 'sink' in icon:
            self.img.draw_img_centered(canvas, xoff, yoff + int(animation_step), 3, 3, icon, brightness)
        elif 'rise' in icon:
            self.img.draw_img_centered(canvas, xoff, yoff + num_animation_steps - int(animation_step), 3, 3, icon, brightness)
        else:
            # draw nothing, for less clutter
            pass
