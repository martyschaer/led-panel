#!/usr/bin/python3
"""
Main file, which runs the loop and draws the to matrix.
"""
import sys
import time

from rgbmatrix import RGBMatrix, RGBMatrixOptions

from clockcomponent import ClockComponent
from config import Config
from iconcomponent import IconComponent
from imgutils import ImgUtils
from temperaturecomponent import TemperatureComponent
from textutils import TextUtils
from weatherservice import WeatherService
from layout import top_top_bottom, top_bottom_bottom, left_left_right, left_right_right
import random


class LEDPanel(object):
    def __init__(self):
        self.config = Config()

        options = RGBMatrixOptions()

        options.rows = 32
        options.cols = 32
        options.chain_length = 1
        options.parallel = 1
        options.row_address_type = 0
        options.multiplexing = 0
        options.pwm_bits = 11
        options.brightness = 50  # % brightness
        options.pwm_lsb_nanoseconds = 50
        options.led_rgb_sequence = "RGB"
        options.pixel_mapper_config = ""  # ex. "Rotate:90"

        options.gpio_slowdown = 3  # if you have problems with flicker, try 1, 2, or 3

        self.matrix = RGBMatrix(options=options)

        self.components = list()

        self.canvas = self.matrix.CreateFrameCanvas()
        self.service = WeatherService(self.config)
        self.img = ImgUtils(self.config)
        self.text = TextUtils(self.config)

        clock = ClockComponent(self.config, self.service)
        icon = IconComponent(self.config, self.service)
        temp = TemperatureComponent(self.config, self.service)

        self.components.append(clock)
        self.components.append(icon)
        self.components.append(temp)

    def process(self):
        try:
            self.run()
        except KeyboardInterrupt:
            sys.exit(0)

    def run(self):
        layouts = [top_top_bottom, top_bottom_bottom, left_left_right, left_right_right]
        layout = random.choice(layouts)
        animation_counter = 0
        while True:
            # Clear previous canvas
            self.canvas.Clear()

            # switch layout every 100 frames (roughly 80 seconds)
            # to prevent LEDs from getting too hot
            if animation_counter % 100 == 0:
                layout = random.choice(layouts)

            # call the draw function on all components
            for i in range(0, len(self.components)):
                component = self.components[i]
                component.draw(self.canvas, layout[i], animation_counter)

            # Draw canvas to matrix
            self.canvas = self.matrix.SwapOnVSync(self.canvas)

            # Update Animation Counter
            animation_counter += 1

            # Sleep until next frame
            time.sleep(.8)


if __name__ == "__main__":
    led_panel = LEDPanel()
    led_panel.process()
