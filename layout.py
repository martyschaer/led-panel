def __create(x, y, w, h):
    return {'x': x, 'y': y, 'w': w, 'h': h}


__top_left = __create(0, 0, 16, 16)
__top_right = __create(16, 0, 16, 16)
__bottom_left = __create(0, 16, 16, 16)
__bottom_right = __create(16, 16, 16, 16)
__vertical_left = __create(0, 0, 16, 32)
__vertical_right = __create(16, 0, 16, 32)
__horizontal_top = __create(0, 0, 32, 16)
__horizontal_bottom = __create(0, 16, 32, 16)

"""
+--+--+
|  |  |
+--+--+
|  |  |
+--+--+
"""
four_squares = [
    __top_left,
    __top_right,
    __bottom_left,
    __bottom_right
]

"""
+--+--+
|  |  |
+  +--+
|  |  |
+--+--+
"""
left_right_right = [
    __vertical_left,
    __top_right,
    __bottom_right
]

"""
+--+--+
|  |  |
+--+  +
|  |  |
+--+--+
"""
left_left_right = [
    __vertical_right,
    __top_left,
    __bottom_left
]

"""
+--+--+
|  |  |
+--+--+
|     |
+--+--+
"""
top_top_bottom = [
    __horizontal_bottom,
    __top_left,
    __top_right
]

"""
+--+--+
|     |
+--+--+
|  |  |
+--+--+
"""
top_bottom_bottom = [
    __horizontal_top,
    __bottom_left,
    __bottom_right
]
