"""
Class which provides simple access to the configuration.
"""
import json
import os


class Config(object):
    def __init__(self, config_path='config.json'):
        self.__dir = os.path.dirname(__file__) + '/'
        with open(self.path(config_path), 'r') as config_file:
            self.config = json.load(config_file)

    def dir(self):
        return self.__dir

    def path(self, file):
        return self.__dir + file

    def __get_or_default(self, key, default):
        if key not in self.config:
            return default
        return self.config[key]

    """
    Returns the font file used to draw text.
    Defaults to the included Tom Thumb Font.
    It's a monospaced 3x5 pixel font from
    https://robey.lag.net/2010/01/23/tiny-monospace-font.html
    by Brian Swetland and Robey Pointer
    """

    def font(self):
        return self.__get_or_default('font', self.path('res/tom_thumb.bdf'))

    """
    Returns the API key for openweathermap.org.
    """

    def owm_api_key(self):
        return self.__get_or_default('owm_api_key', 'invalid-key')

    """
    Returns the city-id for openweathermap.org.
    Find it by going to openweathermap.org and searching for your city,
    then copy the ID from the URL.
    Defaults to Bern, Switzerland
    """

    def owm_city_id(self):
        return self.__get_or_default('owm_city_id', '2661552')

    """
    Returns the Timezone this system should use.
    Defaults to UTC
    """

    def timezone(self):
        return self.__get_or_default('timezone', 'UTC')

    """
    Returns the latitude this system should use.
    Defaults to 0.0
    """

    def coords_lat(self):
        return float(self.__get_or_default('lat', '0.0'))

    """
    Returns the longitude this system should use.
    Defaults to 0.0
    """

    def coords_lon(self):
        return float(self.__get_or_default('lon', '0.0'))

    """
    Returns the refresh interval for API calls.
    Given in [Minutes]
    Defaults to 15
    """

    def api_refresh_interval(self):
        return int(self.__get_or_default('api_refresh_interval', '15'))
